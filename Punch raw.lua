local Exec_Key = 'n' -- key to enable / disable auto punch ()...
local Type = "Right" --[[ 'Right' for Right click ()...
                         'Left' for Left Click        --]]
check = true
local plr = game:getService("Players").LocalPlayer
local Mouse = plr:GetMouse()
x = 0 
-- starter gui
game.StarterGui:SetCore("SendNotification", {
Title = "Auto Punch";
Text = "Press N to enable/disable, dont use in queue",
Icon = "rbxassetid://5472203252";
Duration = 6;
})
Mouse.KeyDown:connect(function(Key)
 Key = Key:lower()
if Key == Exec_Key then
	
	-- to swap on / off
    if check == false then
        check = true
    end
    if x == 1 then
		game:GetService("StarterGui"):SetCore("SendNotification", {
				Title = "Auto Punch";
				Text = "OFF";
				})     
        check = false
        x = 0
    end
 

	if check == true then	
		x = 1
		game:GetService("StarterGui"):SetCore("SendNotification", {
				Title = "Auto Punch";
				Text = "ON";
				})     
		 -- auto punch code 
			if Type == "Right" then
				while ( check == true ) do
					local x = {	[1] = "m2" }
					local frames = CFrame.new(-189.383453, 59.6394577, -73.3549271, 0.159321189, -0.327645749, 0.931270778, -0, 0.943320036, 0.33188498, -0.987226844, -0.0528763086, 0.150290847)
					local Punch = plr.Backpack.ServerTraits.Input
					Punch:FireServer(x, frames, nil, false)
					wait(0,1)
				end
			end
			if Type == "Left" then
				while ( check == true ) do
						local A_1 =  { [1] = "mx" }
						local A_2 = CFrame.new(-457.372772, 27.8608284, -6446.6377, 0.991751373, -0.0145149687, 0.127352476, 9.31322575e-10, 0.993567526, 0.11324162, -0.128176972, -0.112307534, 0.985371888)
						local Event = plr.Backpack.ServerTraits.Input
						Event:FireServer(A_1, A_2, nil,false)
					wait(0.8)
				end
			end
	end		

end
end)
